Game-ready
(saying, well, technically:)

Hard dependencies:
- xOBSE https://github.com/llde/xOBSE
- MenuQue https://www.nexusmods.com/oblivion/mods/32200?tab=files
- AddActorValues https://www.nexusmods.com/oblivion/mods/33248?tab=files
- NifSE https://www.nexusmods.com/oblivion/mods/21292

Soft dependencies:
- CameraCommands by Alenet